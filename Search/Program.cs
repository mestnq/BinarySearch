﻿using System;

namespace Search
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] data = {1, 32, 54, 65, 87, 98, 111, 123};
            Console.WriteLine("Какое число вы хотите найти");
            int searchNumber = Convert.ToInt32(Console.ReadLine());
            int leftIndex = 0;
            int rightIndex = data.Length - 1;
            Console.WriteLine(Make(leftIndex, rightIndex, data, searchNumber));
        }

        static int Make(int leftIndex, int rightIndex, int[] data, int searchNumber)
        {
            int markerIndex = -1;
            while (leftIndex <= rightIndex)
            {
                markerIndex = (leftIndex + rightIndex) / 2;
                if (data[markerIndex] == searchNumber)
                {
                    return markerIndex;
                }

                if (data[markerIndex] < searchNumber)
                {
                    leftIndex = markerIndex + 1;
                }
                else if (data[markerIndex] > searchNumber)
                {
                    rightIndex = markerIndex - 1;
                }
            }

            return markerIndex;
        }
    }
}